package com.faysal.adnow.network

import com.faysal.adnow.models.CategoriesResponse
import com.faysal.adnow.models.ads.AdsResponse
import com.faysal.adnow.models.thumbnial.Thumbnial
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiService {
    @GET("oc-content/plugins/rest/api.php?key=test&type=read&object=category&action=root")
    suspend fun getCategories() : Response<CategoriesResponse>
//
//    @GET("oc-content/plugins/rest/api.php?key=test&type=read&object=search&action=items&sOrder=dt_pub_date&iOrderType=desc")
//    suspend fun getLocations(
//        @Query("key") key : String = "test",
//        @Query("type") type : String = "read",
//        @Query("object") objects : String = "search",
//        @Query("action") action : String = "items",
//        @Query("sOrder") order : String = "dt_pub_date",
//        @Query("iOrderType") order_type : String = "desc",
//        @Query("sCategory") sub_category : String = "",
//        @Query("sRegion") region : String = "",
//    ) : Response<Ads>

    @GET("oc-content/plugins/rest/api.php?")
    suspend fun getAdsInfo(
        @QueryMap queryMap: Map<String,String>
    ) : Response<AdsResponse>

    //key=test&type=read&object=item&action=resourcesById&itemId=3007S
    @GET("oc-content/plugins/rest/api.php?")
    fun getThumbnialById(
        @Query("key") key : String = "test",
        @Query("type") type : String = "read",
        @Query("object") objects : String = "item",
        @Query("action") action : String = "resourcesById",
        @Query("itemId") itemId : String = "itemId",
    ) : Call<Thumbnial>



  /*  @GET("oc-content/plugins/rest/api.php?")
    suspend fun getLocations(
        @Query("key") key : String = "test",
        @Query("type") type : String = "read",
        @Query("object") objects : String = "region",
        @Query("action") action : String = "listAll",
    ) : Response<LocationResponse>

    @GET("oc-content/plugins/rest/api.php?")
    suspend fun getRegions(
        @Query("key") key : String = "test",
        @Query("type") type : String = "read",
        @Query("object") objects : String = "cities",
        @Query("action") action : String = "byRegionId",
        @Query("regionId") regionId : String = "23",
    ) : Response<RegionResponse>*/

}