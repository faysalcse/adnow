package com.faysal.adnow.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.faysal.adnow.models.CategoriesResponse
import com.faysal.adnow.models.ads.AdsResponse
import com.faysal.adnow.models.thumbnial.Thumbnial
import com.faysal.adnow.repository.NetworkRepo
import kotlinx.coroutines.Dispatchers
import retrofit2.Response

class MainViewModel : ViewModel() {
    val repository: NetworkRepo = NetworkRepo()

    fun getAllPost() : LiveData<CategoriesResponse> {
        return liveData(Dispatchers.IO) {
            val response = repository.getCategories()
            emit(response.body()!!)
        }
    }

    fun getAds(queryMap: Map<String,String>) : LiveData<Response<AdsResponse>> {
        return liveData(Dispatchers.IO) {
            val response = repository.getAds(queryMap)
            emit(response)
        }
    }




/*    private val _CategorisUiState = MutableStateFlow<CategoriesUiState>(CategoriesUiState.Empty)
    public val mainUiState : StateFlow<CategoriesUiState> = _CategorisUiState


    fun getAllCategories() = viewModelScope.launch {
        _CategorisUiState.value = CategoriesUiState.Loading
        val response = repository.getCategories()
        if (response.isSuccessful) {
            val cate = response.body()?.response
            if (cate?.size == 0) {
                _CategorisUiState.value = CategoriesUiState.Empty
            } else {
                _CategorisUiState.value = CategoriesUiState.Success(cate)
            }
        }else{
            _CategorisUiState.value = CategoriesUiState.Error("Something wrong data not found !")
        }
    }

    sealed class CategoriesUiState {
        data class Success(val message: List<Response>?)  : CategoriesUiState()
        data class Error(val message: String) : CategoriesUiState()
        object Loading : CategoriesUiState()
        object Empty : CategoriesUiState()

    }*/


}