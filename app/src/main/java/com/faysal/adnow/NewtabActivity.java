package com.faysal.adnow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.faysal.adnow.databinding.ActivityNewtabBinding;
import com.example.dhirbundle.DhirView;


public class NewtabActivity extends AppCompatActivity {


    DhirView browser;

    TextView tabtitle;



    ActivityNewtabBinding binding;

    private SwipeRefreshLayout myswiperfreshlayout;
    private RelativeLayout errorlayout;


    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        browser.onActivityResult(requestCode,resultCode,intent);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityNewtabBinding.inflate(getLayoutInflater(),null,false);

        setContentView(binding.getRoot());


        errorlayout = (RelativeLayout)findViewById(R.id.errorlayout);



        myswiperfreshlayout = (SwipeRefreshLayout)findViewById(R.id.newtabswiperefresh);

        int c1 = getResources().getColor(R.color.colorPrimaryDark);

        myswiperfreshlayout.setColorSchemeColors(c1, c1,c1);
        myswiperfreshlayout.setOnRefreshListener(() -> browser.reload());


        tabtitle = findViewById(R.id.pagetitle);
        browser = findViewById(R.id.newtabbrowser);


        browser.setBaseurl(com.faysal.adnow.Constants.baseurl);
        browser.setDhirviewListener(new DhirView.DhirviewListener() {
            @Override
            public void setError(boolean state) {
                errorlayout.setVisibility(state?View.VISIBLE:View.GONE);
            }

            @Override
            public void setNavbarVisibility(boolean state) {

            }

            @Override
            public void setRefreshable(boolean state) {
                browser.setRefreshavailable(state);
            }

            @Override
            public void isLoading(boolean state) {

                if (state){
                    binding.midprog.setAlpha(1);
                    binding.midprog.setVisibility(View.VISIBLE);
                }else {
                    binding.midprog.animate().alpha(0).setDuration(300).setListener(new AnimatorListenerAdapter(){
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            binding.midprog.setVisibility(View.GONE);
                            binding.midprog.setAlpha(1);
                        }
                    });
                }
            }

            @Override
            public void setTitle(String title) {
                tabtitle.setText(title);
            }

            @Override
            public void pageAppeared() {
                tabtitle.setText("Loading...");
            }
        });

        browser.setNewTab(this.getClass());
        browser.setup();
        browser.setRefreshavailable(false);

        WebSettings ezwebset = browser.getSettings();

        ezwebset.setUserAgentString("Mozilla/5.0 (Linux; U; Android 9; en-gb; Redmi Note 5 Build/PKQ1.180904.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/61.0.3163.128 Mobile Safari/537.36 XiaoMi/Mint Browser/3.4.0; tdsecondwindow");


        new DhirView.ViewHandler(this,binding.getRoot(),binding.midprog);




        if (getIntent()!=null){

            browser.loadUrl(getIntent().getDataString());

        }


    }



    @Override
    public void onBackPressed() {
        if (browser.canGoBack()){
            browser.goBack();
        }else {
            super.onBackPressed();
        }
    }

    public void closewindow(View view) {

        finish();
    }


    public void tryagain(View v){
        browser.reload();
    }







}
