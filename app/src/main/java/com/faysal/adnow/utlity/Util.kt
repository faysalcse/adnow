package com.faysal.adnow.utlity

import android.content.Context
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*


object Util {
    private const val TAG = "Util"
    fun rupeeFormat(valueD: String): String? {
        Log.d(TAG, "rupeeFormat: " + valueD)
        var value = removeLastChars(valueD, 6)
        value = value!!.replace(",", "")
        val lastDigit = value[value.length - 1]
        var result = ""
        val len = value.length - 1
        var nDigits = 0
        for (i in len - 1 downTo 0) {
            result = value[i].toString() + result
            nDigits++
            if (nDigits % 2 == 0 && i > 0) {
                result = ",$result"
            }
        }
        return result + lastDigit
    }

    //2020-12-10 23:49:47
    fun formatTimeStamp(timstamp: String, context: Context) : String{
//        val date: Date = DateTimeUtils.formatDate(timstamp)
//        val timeAgo = DateTimeUtils.getTimeAgo(context, date, DateTimeStyle.MEDIUM)
        return "5h";
    }

    fun removeLastChars(str: String, chars: Int): String? {
        return str.substring(0, str.length - chars)
    }
}