package com.faysal.adnow;

/**
 * Created by dhir on 25-Jan-19.
 */


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


//class extending FirebaseMessagingService
public class MyFirebaseMessagingService extends FirebaseMessagingService {



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {




        //getting the title and the body
        String title,body;
        title = remoteMessage.getNotification().getTitle();
        body = remoteMessage.getNotification().getBody();

        //then here we can use the title and body to build a notification


        Log.d("clmsg",title+body);
        Log.d("onmsgrcvd","called");


        /*
         * If the device is having android oreo we will create a notification channel
         * */


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, importance);
            mChannel.setDescription(Constants.CHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            mChannel.setShowBadge(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(mChannel);

        }


        String imageurl=null;

        try {
            imageurl = remoteMessage.getNotification().getImageUrl().toString();
        }catch (Exception e){

        }

        Log.d("logevent","img:"+imageurl);

        /*
         * Displaying a notification locally
         */
        //if the message contains data payload
        //It is a map of custom keyvalues
        //we can read it easily
        if(remoteMessage.getData().size() > 0){
            //handle the data message here
            String url= remoteMessage.getData().get("url").toString();



            MyNotificationManager.getInstance(this).displayNotification(title, body,url,imageurl);

        }else {

            MyNotificationManager.getInstance(this).displayNotification(title, body,null,imageurl);
        }


        super.onMessageReceived(remoteMessage);

    }

}