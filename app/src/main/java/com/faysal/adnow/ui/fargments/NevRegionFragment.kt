package com.faysal.adnow.ui.fargments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.faysal.adnow.MainActivityCurrent
import com.faysal.adnow.R
import com.faysal.adnow.models.*
import com.faysal.adnow.repository.NetworkRepo
import com.faysal.adnow.ui.adapters.RegionAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_nev_location.*
import org.json.JSONException
import org.json.JSONObject


class NevRegionFragment : Fragment(R.layout.nav_fragment_region) {

    private var mainvew: View ? = null
    lateinit var regionAdapter: RegionAdapter
    lateinit var networkRepo: NetworkRepo

    private val TAG = "NevRegionFragment"

    private val args : NevRegionFragmentArgs by navArgs()



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainvew = view
        (activity as MainActivityCurrent?)?.hideToolbar()
        regionAdapter = RegionAdapter(findNavController())
        networkRepo = NetworkRepo()

        toolbar_title.text = args.name


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        list_view.adapter = regionAdapter
        list_view.layoutManager = LinearLayoutManager(activity)
        list_view.setHasFixedSize(true)

        val dividerItemDecoration = DividerItemDecoration(
            list_view.getContext(),
            LinearLayoutManager.VERTICAL
        )
        list_view.addItemDecoration(dividerItemDecoration)

        backBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp();
        }


        initalizeLoactionView()

        search_src_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                regionAdapter.filter.filter(s)
            }
        })

        searchBtn.setOnClickListener {
            val text  = search_src_text.text.toString()
            regionAdapter.filter.filter(text)
        }

    }

    fun initalizeLoactionView() {

        val data = loadJSONFromAsset(args.key)
        regionAdapter.setList(data)

    }


    fun loadJSONFromAsset(key : String) : ArrayList<SavedData>? {
        try {
            val objectArrayString: String = resources.openRawResource(R.raw.cities).bufferedReader().use { it.readText() }
            val cities = JSONObject(objectArrayString).getJSONObject("cities")
            val state = cities.getJSONArray(key)

            val list = ArrayList<SavedData>()

            for (i in 0 until state.length()){
                val savedRegion = Gson().fromJson(state.getJSONObject(i).toString(),SavedData::class.java)
                list.add(savedRegion)
            }
            return list
        } catch (e: JSONException) {
            e.printStackTrace()
            return null
        }
    }

}