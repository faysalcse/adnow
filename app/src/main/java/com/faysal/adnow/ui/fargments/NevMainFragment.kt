package com.faysal.adnow.ui.fargments

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.faysal.adnow.MainActivityCurrent
import com.faysal.adnow.R
import com.faysal.adnow.helpers.Constans
import com.faysal.adnow.models.ads.Ads
import com.faysal.adnow.models.ads.AdsResponse
import com.faysal.adnow.ui.adapters.AdsAdapter
import com.faysal.adnow.ui.adapters.MainCategoriesAdapter
import com.faysal.adnow.utlity.OnHymnClickListener
import com.faysal.adnow.viewmodels.MainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_nev_main.*
import retrofit2.Response


class NevMainFragment : Fragment(R.layout.fragment_nev_main) , OnHymnClickListener {

    lateinit var queryMap: HashMap<String, String>
    lateinit var categories_adapter: MainCategoriesAdapter
    lateinit var adsAdapter: AdsAdapter

    lateinit var mainViewModel: MainViewModel

    private val TAG = "NevMainFragment"

    lateinit var mainView: View


    var page = 0
    var isLoading = false
    val limit = 30

    lateinit var layout_manager: LinearLayoutManager

    private val navigation_args: NevMainFragmentArgs by navArgs()

    var shimmerFrameLayout : ShimmerFrameLayout? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainView = view;
        shimmerFrameLayout = mainView.findViewById(R.id.shimmer_frame_layout)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        categories_adapter = MainCategoriesAdapter()
        home_categories.layoutManager =
            LinearLayoutManager(activity).apply { orientation = LinearLayoutManager.HORIZONTAL }
        categories_adapter.setList(Constans.getStaticCategories())
        home_categories.adapter = categories_adapter
        layout_manager =
            LinearLayoutManager(activity).apply { orientation = LinearLayoutManager.VERTICAL }
        categories_adapter.setListener(this)

        adsAdapter = AdsAdapter(findNavController())
        ads_list.apply {
            adapter = adsAdapter
            layoutManager = layout_manager
            hasFixedSize()
        }



        btn_location.setOnClickListener {
            val action = NevMainFragmentDirections.actionNevMainFragmentToNevLocationFragment()
            findNavController().navigate(action)
        }

        btn_categories.setOnClickListener {
            val action = NevMainFragmentDirections.actionNevMainFragmentToNavCategoriesFragment()
            findNavController().navigate(action)
        }

        btn_filter.setOnClickListener {
            val action = NevMainFragmentDirections.actionNevMainFragmentToNevFilterFragment()
            findNavController().navigate(action)
        }


        ads_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (dy > 0) {
                    val visibleItemCount = layout_manager.childCount
                    val pastVisibleItem = layout_manager.findFirstCompletelyVisibleItemPosition()
                    val total = adsAdapter.itemCount

                    if (!isLoading) {

                        if ((visibleItemCount + pastVisibleItem) >= total) {
                            page = page + 1
                            getPageById()
                        }

                    }
                }

                super.onScrolled(recyclerView, dx, dy)
            }
        })

        search_btn.setOnClickListener {
            val search_query = search_input.text.toString().trim()

            if (TextUtils.isEmpty(search_query) || search_query == null){
                val snackbar = Snackbar
                    .make(mainView, getString(R.string.enter_your_search_query), Snackbar.LENGTH_LONG)
                snackbar.show()
            }else{
                queryMap.put("sPattern",search_query)
                actionForItems(queryMap)
                search_input.text.clear();
            }


        }


    }

    public fun actionForItems(map: HashMap<String, String>) {
        lifecycleScope.launchWhenStarted {
            shimmer_frame_layout.startShimmerAnimation()
            shimmerFrameLayout?.visibility = View.VISIBLE
            ads_list.visibility = View.GONE
            adsAdapter.clearAllList()
            mainViewModel.getAds(map).observe(viewLifecycleOwner, {
                parseResponse(it)
                shimmer_frame_layout.stopShimmerAnimation()
                shimmerFrameLayout?.visibility = View.GONE
                ads_list.visibility = View.VISIBLE
            })
        }

    }


    private fun getPageById() {
        lifecycleScope.launchWhenStarted {
            isLoading = true
            loadMore.visibility = View.VISIBLE

            queryMap.put("iPage", "" + page)

            mainViewModel.getAds(queryMap).observe(viewLifecycleOwner, {
                parseResponse(it)
                isLoading = false
                loadMore.visibility = View.GONE
            })
        }
    }


    private fun parseResponse(response: Response<AdsResponse>) {

        if (response.isSuccessful == true) {
            val adsList: List<Ads> = response.body()!!.response

            Log.d(TAG, "parseResponse: " + adsList.size)

            if (adsList.size == 0) {
                val snackbar = Snackbar
                    .make(mainView, getString(R.string.nodatafoud), Snackbar.LENGTH_LONG)
                snackbar.show()
            } else {
                adsAdapter.setList(adsList as ArrayList<Ads>)
            }


        } else {
            Log.d(TAG, "parseResponse: Failed")
        }

    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivityCurrent?)?.showToolbar()

        try {

            queryMap = Constans.getQueryMap()
            adsAdapter.clearAllList()

            queryMap.put("sCategory", navigation_args.subcategoryId!!)
            queryMap.put("sRegion", navigation_args.regionId!!)


            Constans.subcategory__id = navigation_args.subcategoryId!!
            Constans.subcategory__name = navigation_args.subCategoryName!!
            Constans.region_id = navigation_args.regionId!!
            Constans.region__name = navigation_args.regionName!!


            val subcategoryName = navigation_args.subCategoryName

            if (subcategoryName !="" && subcategoryName != null ){
                btn_categories.text =subcategoryName
            }



            val regionName = navigation_args.regionName

            if (regionName !="" && regionName != null ){
                btn_location.text = regionName
            }


            actionForItems(queryMap)

        } catch (excption: Exception) {
            excption.printStackTrace()
            queryMap = Constans.getQueryMap()
            actionForItems(queryMap)
        }


    }

    override fun onHymnClick(hymnModel: Int) {
        val id = categories_adapter.getIdByPosition(hymnModel)
        queryMap.put("sCategory",id!!)
        actionForItems(queryMap)
        Log.d(TAG, "onHymnClick: "+id)
    }


}

