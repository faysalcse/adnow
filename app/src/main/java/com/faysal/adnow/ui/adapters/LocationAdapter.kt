package com.faysal.adnow.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.faysal.adnow.R
import com.faysal.adnow.models.Region
import com.faysal.adnow.ui.fargments.NevLocationFragmentDirections
import java.lang.Character.toLowerCase
import java.util.*
import kotlin.collections.ArrayList

class LocationAdapter(val navController: NavController) : RecyclerView.Adapter<LocationAdapter.LocationHolder>(),
    Filterable {

     var list: List<Region> = ArrayList()
     lateinit var context : Context

    var regionList = ArrayList<Region>()


    class LocationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: View = itemView
        var location_name: TextView

        init {
            location_name = view.findViewById(R.id.location_name);
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationHolder {
        val inflater = LayoutInflater.from(parent.context)
        return LocationHolder(
            inflater.inflate(
                R.layout.item_location,
                parent,
                false
            )
        )

    }

    private  val TAG = "LocationAdapter"

    override fun onBindViewHolder(holder: LocationHolder, position: Int) {
        val categories : Region = list.get(position)
        holder.location_name.text = categories.name

        holder.view.setOnClickListener {
//            val action = Direction.actionLoginFragmentToWelcomeFragment(username, password)
//            findNavController().navigate(action)
            val action = NevLocationFragmentDirections.actionNevLocationFragmentToNevRegionFragment(
                categories.name,
                categories.id,
                categories.key
            )
            navController.navigate(action)
        }


    }

    override fun getItemCount(): Int {
       return list.size
    }

    public fun setList(alldata: ArrayList<Region>){
        list = alldata
        regionList = alldata
        notifyDataSetChanged()
    }


    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    list = regionList
                } else {
                    val resultList = ArrayList<Region>()
                    for (row in regionList) {
                        if (row.name.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    list = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = list
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                list = results?.values as ArrayList<Region>
                notifyDataSetChanged()
            }

        }
    }





}