package com.faysal.adnow.ui.fargments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.faysal.adnow.MainActivityCurrent
import com.faysal.adnow.R
import com.faysal.adnow.helpers.Constans
import com.faysal.adnow.ui.adapters.LocationAdapter
import kotlinx.android.synthetic.main.fragment_nev_location.*


class NevLocationFragment : Fragment(R.layout.fragment_nev_location) {

    private var mainvew: View ? = null
    lateinit var locationAdapter: LocationAdapter
    private val TAG = "NevLocationFragment"



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainvew = view
        (activity as MainActivityCurrent?)?.hideToolbar()
        locationAdapter = LocationAdapter(findNavController())


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        list_view.adapter = locationAdapter
        list_view.layoutManager = LinearLayoutManager(activity)
        list_view.setHasFixedSize(true)

        val dividerItemDecoration = DividerItemDecoration(
            list_view.getContext(),
            LinearLayoutManager.VERTICAL
        )
        list_view.addItemDecoration(dividerItemDecoration)

        backBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp();
        }

       initalizeLoactionView()

        search_src_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                locationAdapter.filter.filter(s)
            }
        })

        searchBtn.setOnClickListener {
            val text  = search_src_text.text.toString()
            locationAdapter.filter.filter(text)
        }
    }

    fun initalizeLoactionView() {
        val list = Constans.getRegion()
        locationAdapter.setList(list)

    }




}