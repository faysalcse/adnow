package com.faysal.adnow.ui.fargments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.faysal.adnow.MainActivityCurrent
import com.faysal.adnow.R
import com.faysal.adnow.helpers.Constans
import com.faysal.adnow.repository.NetworkRepo
import com.faysal.adnow.ui.adapters.CategoryAdapter
import kotlinx.android.synthetic.main.fragment_nev_location.*


class NevCategoriesFragment :  Fragment(R.layout.fragment_nav_categories) {

    private var mainvew: View ? = null
    lateinit var adapter: CategoryAdapter
    lateinit var networkRepo: NetworkRepo

    private val TAG = "NavCategoriesFragment"



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainvew = view
        (activity as MainActivityCurrent?)?.hideToolbar()
        adapter = CategoryAdapter(findNavController())
        networkRepo = NetworkRepo()
        toolbar_title.text = getString(R.string.select_category)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        list_view.adapter = adapter
        list_view.layoutManager = LinearLayoutManager(activity)
        list_view.setHasFixedSize(true)

        val dividerItemDecoration = DividerItemDecoration(
            list_view.getContext(),
            LinearLayoutManager.VERTICAL
        )
        list_view.addItemDecoration(dividerItemDecoration)

        backBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp();
        }


        initalizeLoactionView()


    }

    fun initalizeLoactionView() {
            val list = Constans.getStaticCategories()
            adapter.setList(list)

        }


}