package com.faysal.adnow.ui.adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.faysal.adnow.R
import com.faysal.adnow.models.ads.Ads
import com.faysal.adnow.models.thumbnial.Thumbnial
import com.faysal.adnow.network.ApiService
import com.faysal.adnow.network.NetworkBuilder
import com.faysal.adnow.ui.fargments.NevCategoriesFragmentDirections
import com.faysal.adnow.ui.fargments.NevMainFragmentDirections
import com.faysal.adnow.utlity.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import androidx.navigation.NavController
import java.lang.Exception


class AdsAdapter(val navController: NavController)  : RecyclerView.Adapter<AdsAdapter.AdsHolder>() {

    private val TAG = "AdsAdapter"

    val list: MutableList<Ads> = mutableListOf()
    val loadedImage: MutableList<Int> = mutableListOf()

    lateinit var context: Context

    var apiService: ApiService = NetworkBuilder.getInstance().create(ApiService::class.java)

    class AdsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: View = itemView
        var thumbnail: ImageView
        var isUrgent: ImageView
        var ads_type: ImageView
        var isSold: ImageView
        var location_name: TextView
        var ads_title: TextView
        var price: TextView
        var timestamp: TextView
        var main_card: CardView

        init {
            main_card = view.findViewById(R.id.main_card);

            thumbnail = view.findViewById(R.id.thumbnail);
            isUrgent = view.findViewById(R.id.isUrgent);
            ads_title = view.findViewById(R.id.title);
            location_name = view.findViewById(R.id.location_name);
            ads_type = view.findViewById(R.id.ads_type);
            isSold = view.findViewById(R.id.isSold);
            price = view.findViewById(R.id.price);
            timestamp = view.findViewById(R.id.timestamp);


        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdsHolder {
        context = parent.context
        val inflater = LayoutInflater.from(parent.context)
        return AdsHolder(
            inflater.inflate(
                R.layout.item_ads,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: AdsHolder, position: Int) {
        val ads: Ads = list.get(position)
        holder.ads_title.text = ads.s_title
        (ads.s_city + "," + ads.s_category_name).let { holder.location_name.text = it }

        try {
            ads.i_price?.let { holder.price.text = "Rs." + Util.rupeeFormat(it) }
        } catch (thrable: Throwable) {
            holder.price.text = "Unspecified"
        }

        ads.dt_pub_date?.let { holder.timestamp.text = Util.formatTimeStamp(it, context) }

        ads.b_premium?.let {
            if (ads.b_premium == "1") {
                holder.ads_type.setImageResource(R.drawable.ic_paid)
            } else {
                holder.ads_type.setImageResource(R.drawable.ic_free)
            }
        }

        Log.d(TAG, "onBindViewHolder: " + ads.s_title)



            apiService.getThumbnialById(itemId = ads.fk_i_item_id).enqueue(object :
                Callback<Thumbnial> {
                override fun onResponse(call: Call<Thumbnial>, response: Response<Thumbnial>) {
                    if (response.isSuccessful) {
                        val thumbnial_id = response.body()?.response?.get(0)
                        val thumbnial =
                            "https://adnow.lk/" + thumbnial_id?.s_path + thumbnial_id?.pk_i_id + "_thumbnail." + thumbnial_id?.s_extension

                        Glide
                            .with(context)
                            .load(thumbnial)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.thumbnail);



                    }
                }

                override fun onFailure(call: Call<Thumbnial>, t: Throwable) {
                    t.printStackTrace()
                }

            })

        holder.view.setOnClickListener {

            val action = NevMainFragmentDirections.actionNevMainFragmentToAdsDeatils(ads.fk_i_item_id)
            navController.navigate(action)
        }




        try {
            if (ads.dt_mod_date == null){
                ads.dt_pub_date?.let { holder.timestamp.text = Util.formatTimeStamp(it,context)}
            }else{
                ads.dt_mod_date?.let { holder.timestamp.text = Util.formatTimeStamp(it as String,context)}
            }

        }catch (excption : Exception){
            excption.printStackTrace()
        }


        if (ads.b_premium == "1"){
            holder.main_card.setCardBackgroundColor(context.resources.getColor(R.color.premimum))
            holder.price.visibility = View.GONE
            holder.timestamp.visibility = View.GONE
            holder.ads_type.setImageResource(R.drawable.ic_paid)
        }

//        if (ads.b_premium != "0"){
//            holder.main_card.setCardBackgroundColor(context.resources.getColor(R.color.premimum))
//            holder.price.visibility = View.GONE
//            holder.timestamp.visibility = View.GONE
//            holder.ads_type.setImageResource(R.drawable.ic_paid)
//        }



    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(alldata: MutableList<Ads>) {
        if (alldata != null && alldata.size != 0) {
            list.addAll(alldata)
            notifyDataSetChanged()
        }
    }

    fun clearAllList() {
        if (list.size != 0 || list != null) {
            list.clear()
            Log.d(TAG, "clearAllList: " + list.size)
        }

    }




}