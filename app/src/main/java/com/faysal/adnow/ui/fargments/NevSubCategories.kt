package com.faysal.adnow.ui.fargments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.faysal.adnow.MainActivityCurrent
import com.faysal.adnow.R
import com.faysal.adnow.models.SavedData
import com.faysal.adnow.ui.adapters.SubCategoryAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_nev_location.*
import org.json.JSONException
import org.json.JSONObject

class NevSubCategories : Fragment(R.layout.fragment_nev_sub_categories) {

    private var mainvew: View ? = null
    lateinit var adapter: SubCategoryAdapter

    private val TAG = "NevSubCategories"

    private val args : NevSubCategoriesArgs by navArgs()



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainvew = view
        (activity as MainActivityCurrent?)?.hideToolbar()
        adapter = SubCategoryAdapter(findNavController())

        toolbar_title.text = args.name


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        list_view.adapter = adapter
        list_view.layoutManager = LinearLayoutManager(activity)
        list_view.setHasFixedSize(true)

        val dividerItemDecoration = DividerItemDecoration(
            list_view.getContext(),
            LinearLayoutManager.VERTICAL
        )
        list_view.addItemDecoration(dividerItemDecoration)

        backBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp();
        }

        initalizeLoactionView()


    }

    fun initalizeLoactionView() {

        val data = loadJSONFromAsset(args.key)
        adapter.setList(data)

    }


    fun loadJSONFromAsset(key : String) : ArrayList<SavedData>? {
        try {
            val objectArrayString: String = resources.openRawResource(R.raw.categories).bufferedReader().use { it.readText() }
            val cities = JSONObject(objectArrayString).getJSONObject("categories")
            val state = cities.getJSONArray(key)

            val list = ArrayList<SavedData>()

            for (i in 0 until state.length()){
                val savedRegion = Gson().fromJson(state.getJSONObject(i).toString(), SavedData::class.java)
                list.add(savedRegion)
            }
            return list
        } catch (e: JSONException) {
            e.printStackTrace()
            return null
        }
    }

}