package com.faysal.adnow.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.faysal.adnow.R
import com.faysal.adnow.helpers.Constans
import com.faysal.adnow.models.Region
import com.faysal.adnow.models.SavedData
import com.faysal.adnow.ui.fargments.NevRegionFragment
import com.faysal.adnow.ui.fargments.NevRegionFragmentDirections
import java.util.*
import kotlin.collections.ArrayList

class RegionAdapter(val navController: NavController) : RecyclerView.Adapter<RegionAdapter.RegionHolder>(),Filterable {

    var list = ArrayList<SavedData>()
     lateinit var context : Context


    var regionList = ArrayList<SavedData>()


    class RegionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: View = itemView
        var location_name: TextView

        init {
            location_name = view.findViewById(R.id.location_name);
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegionHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RegionHolder(
            inflater.inflate(
                R.layout.item_location,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RegionHolder, position: Int) {
        val categories : SavedData = list.get(position)
        holder.location_name.text = categories.name

        holder.view.setOnClickListener {
//            val action = Direction.actionLoginFragmentToWelcomeFragment(username, password)
//            findNavController().navigate(action)
            val action = NevRegionFragmentDirections.actionNevRegionFragmentToNevMainFragment(Constans.subcategory__id,Constans.subcategory__name,regionName = categories.name,regionId = categories.id)
            navController.navigate(action)

        }


    }

    override fun getItemCount(): Int {
       return list.size
    }

    @JvmName("setList1")
    public fun setList(alldata: ArrayList<SavedData>?){
        if (alldata != null) {
            list.addAll(alldata)
            regionList = alldata
        }
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    list = regionList
                } else {
                    val resultList = ArrayList<SavedData>()
                    for (row in regionList) {
                        if (row.name.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    list = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = list
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                list = results?.values as ArrayList<SavedData>
                notifyDataSetChanged()
            }

        }
    }
}