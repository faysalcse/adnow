package com.faysal.adnow.ui.fargments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.dhirbundle.DhirView.DhirviewListener
import com.faysal.adnow.R

class NevWebViewFragment : Fragment(R.layout.fragment_nev_web_view) {

    private val args : NevWebViewFragmentArgs by navArgs()

    lateinit var browser: WebView
    lateinit var myswiperfreshlayout : SwipeRefreshLayout




    var newtab: Class<*>? = null


    var baseurl = "adnow.lk"
    var homepage = "https://adnow.lk/search/sOrder,dt_pub_date/iOrderType,desc"
    var web_url = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browser = view.findViewById(R.id.browserview)
        setupWebView()
        myswiperfreshlayout = view.findViewById(R.id.swipeContainer)
        val c1 = resources.getColor(R.color.colorPrimaryDark)
        myswiperfreshlayout.setColorSchemeColors(c1, c1, c1)

        myswiperfreshlayout.setOnRefreshListener { browser.reload() }






        when(args.actionUrl){
            "chat" -> web_url = "https://adnow.lk/im-threads"
            "sell" -> web_url = "https://adnow.lk/post-ad/new"
            "my_ads" -> web_url = "https://adnow.lk/user/items"
            "account" -> web_url = "https://adnow.lk/user/dashboard"
            "home" -> web_url = "https://adnow.lk/search/sOrder,dt_pub_date/iOrderType,desc"
        }

        browser.loadUrl(web_url)


    }

    private fun setupWebView() {
        val ezwebset: WebSettings = browser.getSettings()
        ezwebset.setSupportZoom(false)
        ezwebset.builtInZoomControls = false
        ezwebset.displayZoomControls = false
        ezwebset.useWideViewPort = true
        ezwebset.javaScriptEnabled = true
        ezwebset.domStorageEnabled = true
        ezwebset.allowContentAccess = true
        ezwebset.enableSmoothTransition()
        ezwebset.allowFileAccess = true
        ezwebset.loadsImagesAutomatically = true
        ezwebset.setUserAgentString("Mozilla/5.0 (Linux; U; Android 9; en-gb; Redmi Note 5 Build/PKQ1.180904.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/61.0.3163.128 Mobile Safari/537.36 XiaoMi/Mint Browser/3.4.0; adnowmainwindow")
        ezwebset.setSupportMultipleWindows(true)
        ezwebset.javaScriptCanOpenWindowsAutomatically = true


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    override fun onStart() {
        super.onStart()
    }




}