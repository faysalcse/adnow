package com.faysal.adnow.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.faysal.adnow.R
import com.faysal.adnow.models.Category
import com.faysal.adnow.models.HomeCategories
import com.faysal.adnow.ui.fargments.NevCategoriesFragmentDirections
import com.faysal.adnow.ui.fargments.NevLocationFragmentDirections

class CategoryAdapter(val navController: NavController) : RecyclerView.Adapter<CategoryAdapter.CategoryHolder>() {

     var list: List<HomeCategories> = listOf()
     lateinit var context : Context


    class CategoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: View = itemView
        var name : TextView
        var icon : ImageView

        init {
            name  = view.findViewById(R.id.name);
            icon  = view.findViewById(R.id.icon);
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CategoryHolder(
            inflater.inflate(
                R.layout.item_categories,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CategoryHolder, position: Int) {
        val categories : HomeCategories = list.get(position)
        holder.name .text = categories.name
        holder.icon.setImageResource(categories.thumbnail)


        holder.view.setOnClickListener {

            val action = NevCategoriesFragmentDirections.actionNavCategoriesFragmentToNevSubCategories2(categories.id!!,categories.name,categories.key)
            navController.navigate(action)
        }


    }

    private fun showIconToView(icon: ImageView, id: String) {
         when(id){
             "96"->icon.setImageResource(R.drawable.ic_smartphone)
             "107"->icon.setImageResource(R.drawable.ic_car)
             "108"->icon.setImageResource(R.drawable.ic_house)
             "119"->icon.setImageResource(R.drawable.ic_portfolio)
             "133"->icon.setImageResource(R.drawable.ic_washing_machin)
             "142"->icon.setImageResource(R.drawable.ic_watch)
             "154"->icon.setImageResource(R.drawable.ic_cricket)
             "162"->icon.setImageResource(R.drawable.ic_school)
             "144"->icon.setImageResource(R.drawable.ic_services)
             "168"->icon.setImageResource(R.drawable.ic_graduation_hat)
             "176"->icon.setImageResource(R.drawable.ic_animals)
             "183"->icon.setImageResource(R.drawable.ic_planting)
             "178"->icon.setImageResource(R.drawable.ic_other)
         }
    }

    override fun getItemCount(): Int {
       return list.size
    }

    public fun setList(alldata: ArrayList<HomeCategories>){
        list = alldata
        notifyDataSetChanged()
    }
}