package com.faysal.adnow.ui.fargments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import com.faysal.adnow.MainActivityCurrent
import com.faysal.adnow.R
import kotlinx.android.synthetic.main.fragment_nev_location.*


class NevFilterFragment :  Fragment(R.layout.fragment_nev_filter) {

    private var mainvew: View ? = null
    private val TAG = "NevFilterFragment"



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainvew = view
        (activity as MainActivityCurrent?)?.hideToolbar()



    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        backBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp();
        }

    }

}