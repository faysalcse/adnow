package com.faysal.adnow.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.faysal.adnow.R
import com.faysal.adnow.helpers.Constans
import com.faysal.adnow.models.SavedData
import com.faysal.adnow.ui.fargments.NevCategoriesFragmentDirections
import com.faysal.adnow.ui.fargments.NevSubCategoriesDirections

class SubCategoryAdapter(val navController: NavController) : RecyclerView.Adapter<SubCategoryAdapter.SubCategoryHolder>() {

    val list = ArrayList<SavedData>()
     lateinit var context : Context


    class SubCategoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: View = itemView
        var name : TextView

        init {
            name = view.findViewById(R.id.name);
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCategoryHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SubCategoryHolder(
            inflater.inflate(
                R.layout.item_sub_categories,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SubCategoryHolder, position: Int) {
        val categories : SavedData = list.get(position)
        holder.name.text = categories.name

        holder.view.setOnClickListener {
//            val action = Direction.actionLoginFragmentToWelcomeFragment(username, password)
//            findNavController().navigate(action)
            val action = NevSubCategoriesDirections.actionNevSubCategoriesToNevMainFragment(categories.id,categories.name,Constans.region_id,Constans.region__name)
            navController.navigate(action)
        }


    }

    override fun getItemCount(): Int {
       return list.size
    }

    public fun setList(alldata: ArrayList<SavedData>?){
        if (alldata != null) {
            list.addAll(alldata)
        }
        notifyDataSetChanged()
    }
}