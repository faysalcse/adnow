package com.faysal.adnow.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.faysal.adnow.R
import com.faysal.adnow.models.HomeCategories
import com.faysal.adnow.utlity.OnHymnClickListener


class MainCategoriesAdapter() : RecyclerView.Adapter<MainCategoriesAdapter.MainCategoriesHolder>() {

     var list: List<HomeCategories> = listOf()

    private var listener: OnHymnClickListener? = null

    fun setListener(listener: OnHymnClickListener?) {
        this.listener = listener
    }



    inner class MainCategoriesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: View = itemView
        var category_icon: ImageView

        init {
            category_icon = view.findViewById(R.id.category_icon);
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainCategoriesHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MainCategoriesHolder(
            inflater.inflate(
                R.layout.item_horizentol_categories,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MainCategoriesHolder, position: Int) {
        val categories : HomeCategories = list.get(position)
        holder.category_icon.setImageResource(categories.thumbnail)
        holder.itemView.setOnClickListener(View.OnClickListener {
            if (listener != null) {
                listener!!.onHymnClick(position)
            }
        })

    }

    fun getIdByPosition(position: Int) : String? {
        val homeCategories : HomeCategories = list.get(position)
        return homeCategories.id
    }

    override fun getItemCount(): Int {
       return list.size
    }

    public fun setList(alldata: ArrayList<HomeCategories>){
        list = alldata
    }


}