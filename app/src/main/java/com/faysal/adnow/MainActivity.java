package com.faysal.adnow;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.faysal.adnow.databinding.ActivityMainBinding;
import com.example.dhirbundle.AppRate;
import com.example.dhirbundle.DhirHMenu;
import com.example.dhirbundle.DhirView;
import com.faysal.adnow.ui.fargments.NevWebViewFragment;
import com.google.firebase.iid.FirebaseInstanceId;


public class MainActivity extends AppCompatActivity {

    private DhirView browser;
    public String baseurl = "adnow.lk";
    public String homepage="https://adnow.lk/search/sOrder,dt_pub_date/iOrderType,desc";



    private SwipeRefreshLayout myswiperfreshlayout;

    private RelativeLayout errorlayout;

    private String appname;



    DhirHMenu dhirHMenu;
    DhirHMenu.DhirItem navitem_1,navitem_2,navitem_3,navitem_4,navitem_5;



    ActivityMainBinding binding;
    //***

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor speditor;





    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        browser.onActivityResult(requestCode,resultCode,intent);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater(),null,false);
        setContentView(binding.getRoot());



        sharedPreferences  = getSharedPreferences("firstlaunch", Context.MODE_PRIVATE);
        speditor = sharedPreferences.edit();
        myswiperfreshlayout = (SwipeRefreshLayout)this.findViewById(R.id.swipeContainer);
        int c1 = getResources().getColor(R.color.colorPrimaryDark);

        myswiperfreshlayout.setColorSchemeColors(c1, c1,c1);



        appname = getResources().getString(R.string.app_name);
        errorlayout = (RelativeLayout)findViewById(R.id.errorlayout);

        myswiperfreshlayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        browser.reload();
                    }
                }
        );



        try {
            Log.d("logevent", "app opened");
            Log.d("logevent", FirebaseInstanceId.getInstance().getToken());
        }catch (Exception e){
            //
        }










        //browser functionality
        browser =  findViewById(R.id.browserview);
        browser.setBaseurl(baseurl);
        browser.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 9; en-gb; Redmi Note 5 Build/PKQ1.180904.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/61.0.3163.128 Mobile Safari/537.36 XiaoMi/Mint Browser/3.4.0; adnowmainwindow");

        browser.setDhirviewListener(new DhirView.DhirviewListener() {
            @Override
            public void setError(boolean state) {
                errorlayout.setVisibility(state?View.VISIBLE:View.GONE);
            }

            @Override
            public void setNavbarVisibility(boolean state) {

                if (state){
                    showNavbar();
                }else {
                    hideNavbar();
                }
            }

            @Override
            public void setRefreshable(boolean state) {
                browser.setRefreshavailable(state);
            }

            @Override
            public void isLoading(boolean state) {

                if (state){
                    binding.midprog.setAlpha(1);
                    binding.midprog.setVisibility(View.VISIBLE);
                }else {
                    binding.midprog.animate().alpha(0).setDuration(300).setListener(new AnimatorListenerAdapter(){
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            binding.midprog.setVisibility(View.GONE);
                            binding.midprog.setAlpha(1);
                        }
                    });
                }
            }

            @Override
            public void setTitle(String title) {

            }

            @Override
            public void pageAppeared() {

            }
        });

        browser.setNewTab(NewtabActivity.class);

        browser.setup();




        //deeplink
        if (getIntent().getAction()=="deeplink"){
            String burl = getIntent().getStringExtra("burl");
            if (burl.contains(baseurl)){
                if (!burl.contains(":")){
                    browser.loadUrl("https://"+burl);
                }else {
                    browser.loadUrl(burl);
                }
            }else{
                String eurl = burl;
                if(!burl.contains(":")){
                    eurl = "https://"+burl;
                }
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(eurl));
                startActivity(i);
                browser.loadUrl(homepage);
            }
        }else {
            browser.loadUrl(homepage);
        }
        //deeplink

        //Application rating


        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(com.faysal.adnow.MainActivity.this);

        android.app.AlertDialog dialog = builder.create();

        View rateview = getLayoutInflater().inflate(R.layout.rate_layout,null);


        TextView message = rateview.findViewById(R.id.message_body);

        message.setText(String.format(getResources().getString(R.string.rate_message), getResources().getString(R.string.app_name)));


        builder.setView(rateview);



            new AppRate(this)
                    .setShowIfAppHasCrashed(false)
                    .setMinDaysUntilPrompt(1)
                    .setMinLaunchesUntilPrompt(1)
                    .setCustomDialog(builder)
                    .setPositivebutton(rateview.findViewById(R.id.sure_button))
                    .setNutralbutton(rateview.findViewById(R.id.later_button))
                    .setNegativebutton(rateview.findViewById(R.id.no_button))
                    .init();


        int deepaccent = Color.parseColor("#0F2E33");


        dhirHMenu = findViewById(R.id.dhirmenu);
        dhirHMenu.setFont(ResourcesCompat.getFont(this, R.font.agnecy_fb));

        navitem_1 = dhirHMenu.addItem("HOME",R.drawable.ic_home_custom,deepaccent,deepaccent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                browser.loadUrl("https://adnow.lk/search/sOrder,dt_pub_date/iOrderType,desc");

            }
        });

        navitem_2 = dhirHMenu.addItem("CHATS",R.drawable.ic_chats,deepaccent,deepaccent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                browser.loadUrl("https://adnow.lk/im-threads");
            }
        });

        navitem_3 = dhirHMenu.addItem("SELL",R.drawable.ic_notification,deepaccent,deepaccent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browser.loadUrl("https://adnow.lk/post-ad/new");
            }
        }).invisibleIcon().boldLabel();

        ((View)findViewById(R.id.postad)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 browser.loadUrl("https://adnow.lk/post-ad/new");
            }
        });

        navitem_4 = dhirHMenu.addItem("MY ADS",R.drawable.ic_my_ads,deepaccent,deepaccent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                browser.loadUrl("https://adnow.lk/user/items");
            }
        });

        navitem_5 = dhirHMenu.addItem("ACCOUNT",R.drawable.ic_user,deepaccent,deepaccent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                browser.loadUrl("https://adnow.lk/user/dashboard");
            }
        });


        new DhirView.ViewHandler(this,binding.getRoot(),binding.midprog, new View[]{binding.bottomnavbar, binding.bottomnavspace});



    }






    @Override
    protected void onStart() {
        browser.onStart();
        super.onStart();
    }


    public void hideNavbar(){
        browser.navchanging = true;

        binding.bottomnavbar.animate().setDuration(200).translationY(binding.bottomnavbar.getHeight()).setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                browser.navchanging = false;
            }
        });

    }

    public void showNavbar(){
        browser.navchanging = true;
        binding.bottomnavbar.animate().setDuration(200).translationY(0).setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                browser.navchanging = false;
            }
        });

    }








    @Override
    protected void onNewIntent(Intent intent) {
        Intent brint= intent;
        //'''
        if (brint.getAction()=="deeplink"){
            String burl = brint.getStringExtra("burl");
            if (burl.contains(baseurl)){
                if (!burl.contains(":")){
                    browser.loadUrl("https://"+burl);
                }else {
                    browser.loadUrl(burl);
                }
            }else{
                String eurl = burl;
                if(!burl.contains(":")){
                    eurl = "https://"+burl;
                }
                Intent di = new Intent(Intent.ACTION_VIEW);
                di.setData(Uri.parse(eurl));
                startActivity(di);
            }
        }else {
            browser.loadUrl(homepage);
        }

        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {

            if (browser.canGoBack()) {
                browser.goBack();
            } else {

                new AlertDialog.Builder(this)
                        .setTitle("Exit " + appname)
                        .setMessage("Are you sure you want to exit?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                com.faysal.adnow.MainActivity.super.onBackPressed();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //canceled
                            }
                        })
                        .show();

//            }
        }
    }





    public void tryagain(View v){
        browser.reload();
    }





    @Override
    protected void onDestroy() {


        browser.onDestroy();
        super.onDestroy();
    }



    public void tst(String t,int dur){

        if (dur==1){Toast.makeText(getBaseContext(),t,Toast.LENGTH_LONG).show();}else{Toast.makeText(getBaseContext(),t,Toast.LENGTH_SHORT).show();}
    }




}

