package com.faysal.adnow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        final ImageView logo = (ImageView)findViewById(R.id.logo);


        final int anidura = 500;



       final Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Intent resultIntent;

                if (getIntent().getExtras()!=null) {

                    boolean got = false;

                    for (String key : getIntent().getExtras().keySet()){
                        if (key.equals("url")){
                            got=true;
                            String url = getIntent().getExtras().getString(key);

                            resultIntent = new Intent(getBaseContext(), MainActivity.class);
                            /*
                            if (!url.startsWith("http")){url="https://"+url;}
                            Uri uri = Uri.parse(url);
                            resultIntent.setData(uri);
                            */
                            resultIntent.setAction("deeplink");
                            resultIntent.putExtra("burl",url);

                            startActivity(resultIntent);
                            finish();
                        }
                    }
                    if (!got){
                        goHome();
                    }

                }else{
                    goHome();
                }

            }
        };



         final Handler h = new Handler();


                 logo.animate().setDuration(anidura).alpha(1.0f).scaleX(1).scaleY(1).setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new AnimatorListenerAdapter() {
                     @Override
                     public void onAnimationEnd(Animator animation) {

                         h.postDelayed(runnable,500);

                     }
                 });





        }

    public void goHome(){

        Intent home = new Intent(getApplication(), MainActivityCurrent.class);
        startActivity(home);
        finish();
    }

    @Override
    public void onBackPressed() {
        //do nothing
        //super.onBackPressed();
    }
}
