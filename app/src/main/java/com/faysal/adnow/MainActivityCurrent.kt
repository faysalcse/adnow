package com.faysal.adnow

import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.faysal.adnow.databinding.ActivityMainCurrentBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import kotlinx.android.synthetic.main.activity_main_current.*


class MainActivityCurrent : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {

    private val TAG = "MainActivityDDDDDDD"

    lateinit var nevController: NavController


    lateinit var binding: ActivityMainCurrentBinding


    //***
   lateinit var sharedPreferences: SharedPreferences
   lateinit var speditor: SharedPreferences.Editor


    lateinit var myswiperfreshlayout: SwipeRefreshLayout


    lateinit var errorlayout: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainCurrentBinding.inflate(layoutInflater, null, false)
        setContentView(binding.getRoot())

        nevController = Navigation.findNavController(this, R.id.nav_host_fragment)

        sharedPreferences = getSharedPreferences("firstlaunch", MODE_PRIVATE)
        speditor = sharedPreferences.edit()
        myswiperfreshlayout = findViewById(R.id.swipeContainer)
        val c1 = resources.getColor(R.color.colorPrimaryDark)

        myswiperfreshlayout.setColorSchemeColors(c1, c1, c1)

        errorlayout = findViewById(R.id.errorlayout)


        binding.bottomNavigationMain.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        binding.bottomNavigationMain.setOnNavigationItemSelectedListener(this);

        VissableView(View.GONE, "home")

        btn_sell.setOnClickListener {


        }


    }


    fun showToolbar() {
        toolbarLayout.visibility = View.VISIBLE
    }

    fun hideToolbar() {
        toolbarLayout.visibility = View.GONE
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_chats -> VissableView(View.VISIBLE, "chat")

            R.id.menu_home -> VissableView(View.GONE, "home")

            R.id.menu_sell -> VissableView(View.VISIBLE, "sell")

            R.id.menu_my_ads -> VissableView(View.VISIBLE, "my_ads")

            R.id.menu_account -> VissableView(View.VISIBLE, "account")

        }


        //  when (item)

        //   updateNavigationBarState(item.getItemId());

        return true;
    }

    fun VissableView(visibility: Int, webUrl: String = "") {
        if (visibility == View.VISIBLE) {
            binding.webContent.visibility = View.VISIBLE
            binding.fragmentContent.visibility = View.GONE
        } else {
            binding.webContent.visibility = View.GONE
            binding.fragmentContent.visibility = View.VISIBLE
        }
    }

    private fun updateNavigationBarState(actionId: Int) {
        val menu: Menu = binding.bottomNavigationMain.menu
        var i = 0
        val size: Int = menu.size()
        while (i < size) {
            val item: MenuItem = menu.getItem(i)
            item.isChecked = item.itemId == actionId
            i++
        }
    }


}