package com.faysal.adnow;

/**
 * Created by dhir on 27-Jan-19.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.core.app.NotificationCompat;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.Context.NOTIFICATION_SERVICE;


public class MyNotificationManager {

    private Context mCtx;
    private static com.faysal.adnow.MyNotificationManager mInstance;

    private MyNotificationManager(Context context) {
        mCtx = context;
    }

    public static synchronized com.faysal.adnow.MyNotificationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new com.faysal.adnow.MyNotificationManager(context);
        }
        return mInstance;
    }

    public void displayNotification(String title, String body,String url, String imageurl) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mCtx,Constants.CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setAutoCancel(true)
                        .setContentTitle(title)
                        .setContentText(body)
                .setColor(mCtx.getResources().getColor(R.color.colorAccent));


                ;

        if (imageurl!=null){
            mBuilder.setLargeIcon(getBitmapfromUrl(imageurl));
            mBuilder.setStyle(
                    new NotificationCompat.BigPictureStyle()
                            .bigPicture(getBitmapfromUrl(imageurl))
                            .bigLargeIcon(null));

        }


        /*
         *  Clicking on the notification will take us to this intent
         *  Right now we are using the MainActivity as this is the only activity we have in our application
         *  But for your project you can customize it as you want
         * */

        Intent resultIntent;
        if (url!=null) {
            resultIntent = new Intent(mCtx, MainActivity.class);
        /*
            String link = url;
            if (!url.startsWith("http")){link="https://"+url;}
            Uri uri = Uri.parse(link);
            resultIntent.setData(uri);
            */
            resultIntent.setAction("deeplink");
            resultIntent.putExtra("burl",url);
        }else{
            resultIntent = new Intent(mCtx, MainActivity.class);
        }

        /*
         *  Now we will create a pending intent
         *  The method getActivity is taking 4 parameters
         *  All paramters are describing themselves
         *  0 is the request code (the second parameter)
         *  We can detect this code in the activity that will open by this we can get
         *  Which notification opened the activity
         * */
        PendingIntent pendingIntent = PendingIntent.getActivity(mCtx, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /*
         *  Setting the pending intent to notification builder
         * */

        mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotifyMgr =
                (NotificationManager) mCtx.getSystemService(NOTIFICATION_SERVICE);

        /*
         * The first parameter is the notification id
         * better don't give a literal here (right now we are giving a int literal)
         * because using this id we can modify it later
         * */
        if (mNotifyMgr != null) {
            mNotifyMgr.notify(1, mBuilder.build());
        }
    }


    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

}