package com.faysal.adnow.repository

import com.faysal.adnow.network.ApiService
import com.faysal.adnow.network.NetworkBuilder

class NetworkRepo {
    val api_service = NetworkBuilder.getInstance().create(ApiService::class.java)
    suspend fun getCategories() = api_service.getCategories()
    suspend fun getAds(queryMap: Map<String,String>) = api_service.getAdsInfo(queryMap)
    suspend fun getThumbnail(itemId : String) = api_service.getThumbnialById(itemId)

}