package com.faysal.adnow.models.ads

data class AdsResponse(
    val block_id: Int,
    val execution_seconds: Double,
    val message: String,
    val response: List<Ads>,
    val status: String
)