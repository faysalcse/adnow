package com.faysal.adnow.models

data class Region(
    var id: String,
    var name : String,
    var key : String = "ampara"
)
