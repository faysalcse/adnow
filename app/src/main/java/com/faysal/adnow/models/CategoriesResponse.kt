package com.faysal.adnow.models

data class CategoriesResponse(
    val block_id: Int,
    val execution_seconds: Double,
    val message: String,
    val response: List<Category>,
    val status: String
)