package com.faysal.adnow.models.ads

data class EnUS(
    val fk_c_locale_code: String,
    val fk_i_item_id: String,
    val s_description: String,
    val s_title: String
)