package com.faysal.adnow.models

data class Category(
    val b_enabled: String,
    val b_price_enabled: String,
    val fk_c_locale_code: String,
    val fk_i_category_id: String,
    val fk_i_parent_id: Any,
    val i_expiration_days: String,
    val i_num_items: String,
    val i_position: String,
    val pk_i_id: String,
    val s_color: Any,
    val s_description: String,
    val s_icon: Any,
    val s_name: String,
    val s_slug: String
)