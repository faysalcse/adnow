package com.faysal.adnow.models.thumbnial

data class Response(
    val dt_pub_date: String,
    val fk_i_item_id: String,
    val pk_i_id: String,
    val s_content_type: String,
    val s_extension: String,
    val s_name: String,
    val s_path: String
)