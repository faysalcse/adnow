package com.faysal.adnow.models

import com.faysal.adnow.R

data class HomeCategories(var id : String?,var thumbnail : Int = R.drawable.ic_loading,var name : String = "",var key : String)
