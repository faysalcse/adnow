package com.faysal.adnow.models.ads

data class Locale(
    val en_US: EnUS
)