package com.faysal.adnow.models.thumbnial

data class Thumbnial(
    val block_id: Int,
    val execution_seconds: Double,
    val message: String,
    val response: List<Response>,
    val status: String
)