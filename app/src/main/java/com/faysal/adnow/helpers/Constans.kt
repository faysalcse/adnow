package com.faysal.adnow.helpers

import com.faysal.adnow.R
import com.faysal.adnow.models.HomeCategories
import com.faysal.adnow.models.Region


class Constans {
    companion object{

        public const val BASE_URL = "https://adnow.lk/"

        public var region_id : String = ""
        public var region__name: String = "Location"
        public var subcategory__id: String = "Category"
        public var subcategory__name: String = ""

        fun getStaticCategories() : ArrayList<HomeCategories>{
            val list  = ArrayList<HomeCategories>()
            list.add(HomeCategories("96", R.drawable.ic_smartphone, "Electronics","electronics"))
            list.add(HomeCategories("107", R.drawable.ic_car, "Vehicles","vehicles"))
            list.add(HomeCategories("108", R.drawable.ic_house, "Property","property"))
            list.add(HomeCategories("119", R.drawable.ic_portfolio, "Jobs","jobs"))
            list.add(HomeCategories("133", R.drawable.ic_washing_machin, "Home Garden","home_garden"))
            list.add(HomeCategories("142", R.drawable.ic_watch, "Fashion Health Beauty","fashion_health_beauty"))
            list.add(HomeCategories("154", R.drawable.ic_cricket, "Hobby Sport kids","hobby_sports_kids"))
            list.add(HomeCategories("162", R.drawable.ic_school, "Business Industry","buesiness_industry"))
            list.add(HomeCategories("144", R.drawable.ic_services, "Services","service"))
            list.add(HomeCategories("168", R.drawable.ic_graduation_hat, "Education","education"))
            list.add(HomeCategories("176", R.drawable.ic_animals, "Animals","animals"))
            list.add(HomeCategories("183", R.drawable.ic_planting, "Food Agriculture","food_agriculture"))
            list.add(HomeCategories("178", R.drawable.ic_other, "Others","others"))
            return list
        }

        fun getRegion() : ArrayList<Region>{
            val list  = ArrayList<Region>()
            list.add(Region("1","Ampara","ampara"))
            list.add(Region("2","Anuradhapura","anuradhapura"))
            list.add(Region("3","Badulla","badulla"))
            list.add(Region("4","Batticaloa","batticaloa"))
            list.add(Region("5","Gampaha","gampaha"))
            list.add(Region("6","Hambantota","hambantota"))
            list.add(Region("7","Jaffna","jaffna"))
            list.add(Region("8","Kalutara","kalutara"))
            list.add(Region("9","Kegalle","kegalle"))
            list.add(Region("10","Kilinochchi","kilinochchi"))
            list.add(Region("11,","Kurunegala","kurunegala"))
            list.add(Region("12","Mannar","mannar"))
            list.add(Region("13","Matale","matale"))
            list.add(Region("14","Matara","matara"))
            list.add(Region("15","Monaragala","monaragala"))
            list.add(Region("16","Mullaitivu","mullaitivu"))
            list.add(Region("17","Nuwara eliya","nuwara_eliya"))
            list.add(Region("18","Polonnaruwa","polonnaruwa"))
            list.add(Region("19","Puttalam","puttalam"))
            list.add(Region("20","Ratnapura","ratnapura"))
            list.add(Region("21","Trincomalee","trincomalee"))
            list.add(Region("22","Vavuniya","vavuniya"))
            list.add(Region("23","Colombo","colombo"))
            list.add(Region("24","Kandy","kandy"))
            list.add(Region("25","Galle","galle"))

            return list
        }

        fun getQueryMap() : HashMap<String,String>{
            var queryMap = HashMap<String,String>()
            queryMap = HashMap<String, String>()
            queryMap.put("key", "test")
            queryMap.put("type", "read")
            queryMap.put("object", "search")
            queryMap.put("action", "items")
            queryMap.put("sOrder", "dt_pub_date")
            queryMap.put("sCategory", "")
            queryMap.put("iPage", "1")

            return queryMap

        }




    }



}